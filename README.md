# Blendergrid API Example
The purpose of this code is to provide an example on how to access the Blendergrid render farm API with Python to automate rendering.

Check the walkthrough video here: [vimeo.com/blendergrid/api-example](https://vimeo.com/blendergrid/api-example)

## Requirements
You need to have a Blendergrid account with some render credit balance. You can set up an account and buy render credit here: [blendergrid.com/register](https://blendergrid.com/register)

Python 3.6+ is required to run this example.

The following Python packages are required:

- `python-dotenv`
- `requests`

They can be installed with pip:

```
pip install -r requirements.txt
```

Or, inside a virtual environment:

```
python3 -m venv .venv
. .venv/bin/activate
pip install -r requirements.txt
```

## Setting up your credentials
First, create an `.env` file based on the example:

```
cp .env.example .env
```

In this `.env` file, you have to edit the email and password. They have to match your Blendergrid account.

## Rendering a .blend file
To run the example from start to finish, simply execute the main.py file:

```
python3 main.py
```

This will do the following things:
- Use your email/password to get an API access token (and store it in an `.access_token` file)
- Create a **Project Source** entity (this is needed to start uploading source files)
- Upload source files (just a single .blend file)
- Create a **Project** from the **Project Source** and specify which .blend file to use (you can upload multiple .blend files into a project source)
- Wait for the price calculation to finish
- Start rendering using your render credit
- While it's rendering, download all the frames into a `frames` folder
