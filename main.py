import os
import time
from pathlib import Path

import requests
from dotenv import load_dotenv

# Change these to render a different .blend file and store frames in a different folder
BLEND_FILEPATH = "blendergrid-logo.blend"
FRAMES_FOLDER = "frames"

# Load .env file into environment variables
load_dotenv()
EMAIL = os.environ.get("EMAIL")
PASSWORD = os.environ.get("PASSWORD")
API_URL = os.environ.get("API_URL")


def main():
    print(f"Blendergrid API Test - Rendering {BLEND_FILEPATH}")

    access_token = get_access_token()

    project_source_uuid, credentials = create_project_source(access_token)
    upload_source_files(credentials)

    project_uuid = create_project(project_source_uuid, access_token)
    price = wait_for_price_calculation(project_uuid, access_token)

    print(f"\nProject price: ${price:.02} USD")

    start_rendering(project_uuid, access_token)

    sync_frames(project_uuid, access_token)

    print("\nDone")


def get_access_token():
    access_token_filepath = Path(".access_token")

    if access_token_filepath.exists():
        print("\nGetting cached Access Token")

        with open(access_token_filepath, "r") as fp:
            access_token = fp.read()

    else:
        print("\nRequesting a new Access Token")

        response = requests.post(
            f"{API_URL}/users/tokens",
            json={"email": EMAIL, "password": PASSWORD},
        )
        check_response(response)

        access_token = response.json()["token_data"]["access_token"]

        with open(access_token_filepath, "w") as fp:
            fp.write(access_token)

    return access_token


def create_project_source(access_token):
    print("\nCreating a Project Source")

    response = requests.post(
        f"{API_URL}/project-sources",
        json={"type": "custom_api"},
        headers={"Authorization": f"Bearer {access_token}"},
    )
    check_response(response)

    uuid = response.json()["data"]["uuid"]
    credentials = response.json()["data"]["credentials"]
    print(f"Project Source UUID: {uuid}")

    return uuid, credentials


def upload_source_files(credentials):
    print(f"\nUploading .blend file: {BLEND_FILEPATH}")

    s3_url = f"https://{credentials['bucket']}.s3-accelerate.amazonaws.com"
    blend_file = open(BLEND_FILEPATH, "rb")

    response = requests.post(
        s3_url,
        data={
            "key": f"{credentials['key']}{BLEND_FILEPATH}",
            "AWSAccessKeyId": credentials["public_key"],
            "acl": "private",
            "policy": credentials["base64_policy"],
            "signature": credentials["signature"],
            "success_action_status": "201",
        },
        files={"file": blend_file},
    )
    check_response(response)


def create_project(project_source_uuid, access_token):
    print("\nCreating a Project")

    response = requests.post(
        f"{API_URL}/projects",
        json={
            "project_source_uuid": project_source_uuid,
            "projects": [
                {"main_blend_file": BLEND_FILEPATH},
            ],
        },
        headers={"Authorization": f"Bearer {access_token}"},
    )
    check_response(response)

    uuid = response.json()["data"][0]["uuid"]
    print(f"Project UUID: {uuid}")

    return uuid


def wait_for_price_calculation(project_uuid, access_token):
    print("\nWaiting for the Price Calculation to finish")

    latest_event_type = None

    while True:
        response = requests.get(
            f"{API_URL}/projects/{project_uuid}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        response_data = check_response(response)

        previous_event_type = latest_event_type
        latest_event_type = response_data["latest_event_type"]

        if latest_event_type != previous_event_type:
            print(f"\n{latest_event_type}")

        if latest_event_type == "PRICE_CALCULATION_SUCCESS":
            return response_data["price"]

        print(".", end="", flush=True)

        time.sleep(10)


def start_rendering(project_uuid, access_token):
    print("\nStart Rendering (Creating a Render Activity)")

    response = requests.post(
        f"{API_URL}/projects/render-activities",
        json={"projects": [{"uuid": project_uuid}]},
        headers={"Authorization": f"Bearer {access_token}"},
    )
    check_response(response)


def sync_frames(project_uuid, access_token):
    print("\nSyncing frames while rendering")

    frames_folder = Path(FRAMES_FOLDER)
    if not frames_folder.exists():
        frames_folder.mkdir(parents=True)

    latest_event_type = None

    while True:
        response = requests.get(
            f"{API_URL}/projects/{project_uuid}",
            headers={"Authorization": f"Bearer {access_token}"},
        )
        response_data = check_response(response)

        previous_event_type = latest_event_type
        latest_event_type = response_data["latest_event_type"]

        download_project_frames(project_uuid, access_token)

        if latest_event_type != previous_event_type:
            print(f"\n{latest_event_type}")

        if latest_event_type == "RENDER_ACTIVITY_SUCCESS":
            break

        print(".", end="", flush=True)

        time.sleep(10)


def download_project_frames(project_uuid, access_token):
    response = requests.get(
        f"{API_URL}/projects/{project_uuid}/frames",
        headers={"Authorization": f"Bearer {access_token}"},
    )
    response_data = check_response(response)

    downloaded = False
    for frame in response_data["frames"]:
        target = Path(os.path.join(FRAMES_FOLDER, frame["filename"]))

        if target.exists():
            continue

        print(f"\nDownloading {frame['filename']}", end="")
        download_file(frame["url"], target)
        downloaded = True

    if downloaded:
        print()


def download_file(url, target):
    response = requests.get(url, stream=True)

    if response.status_code != 200:
        print(f"Could not download {url}")
        print(f"{response.status_code=}")
        return

    with open(target, "wb") as fp:
        for chunk in response:
            fp.write(chunk)


def check_response(response):
    if response.status_code in (200, 201):
        if "application/json" in response.headers.get("Content-Type", ""):
            return response.json().get("data")

        return {}

    print(f"Something went wrong:")
    if "application/json" in response.headers.get("Content-Type", ""):
        print(response.json().get("errors", response.json()))
    else:
        print(response.text)
    print(f"Response code: {response.status_code}")

    exit(1)


if __name__ == "__main__":
    # exit(main())
    main()
